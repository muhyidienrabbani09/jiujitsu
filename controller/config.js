document.title="Ktupad v3.6";
ktupad.isSignup='show'; // show or hide
ktupad.isDb=1;
ktupad.akses=['c','r','u','d'];
ktupad.periode='2021-09-01 s/d 2021-09-31';

ktupad.dataApp.host='https://sb.simaf.id/jiujitsu/';
ktupad.dataApp.model='../jiujitsu/model/database.php';
ktupad.home='../jiujitsu/controller/master/login.js?login/login';
ktupad.afterlogin='../jiujitsu/controller/jiujitsu/home.js?home/view';
// ktupad.afterlogin='controller/kts/profile.js?profile/view';
// ktupad.afterlogin='controller/aka/absensi.js?absensi/profileGet';
ktupad.temp=ktupad.dataApp.host+'../jiujitsu/view/index.html';

admin=ktupad.getURL('admin');
if(admin){ ktupad.home='../jiujitsu/controller/master/login.js?login/login'; }

c=ktupad.getURL('c');
if(c){ ktupad.home=c; }


ktupad.loadView(ktupad.temp,"myTemplate",function(){
ktupad.loadController(ktupad.home);
});
