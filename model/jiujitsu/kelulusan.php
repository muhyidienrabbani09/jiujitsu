<?php
class mod extends ktupad {
public $conf2= array(
'tb' => 'tb_kelulusan',
'mn' => 'kelulusan',
);

public function lulus(){
$d=$this->conf;
$tb=$d['tb'];
// $induk=$d['induk'];
$conn=$this->connect();
$conn->query("SET SESSION sql_mode = ''");
$data=array();


$result = $conn->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));");

$sql=' SELECT id, id_peserta, tanggal_pengujian, (sum(nilai)/ 2) as nilai_akumulatif, sabuk,
(SELECT Nama From peserta WHERE peserta.id=nilai.id_peserta) as nama,
CASE
    WHEN (sum(nilai)/ 2) > 60 THEN "LULUS"
    ELSE "MENGULANG"
END as lulus FROM nilai group by id_peserta';

$result = $conn->query($sql);
$result->setFetchMode(PDO::FETCH_ASSOC);

$colcount = $result->columnCount();
for ($i = 0; $i < $colcount; $i++) {  $cols = $result->getColumnMeta($i);
  $col[] = $cols['name'];
}


// foreach ($result as $r ) {
//   $sql_insert =" insert into tb_kelulusan set kode_peserta = '$r[kode_peserta]', nama_peserta = '$r[nama]',
//                  nilai_akumulatif = '$r[nilai_akumulatif]', sabuk_peserta = '$r[sabuk]', keterangan_lulus = '$r[lulus]'" ;
//   $conn->query($sql_insert);
// }

while($row = $result->fetch()) { $data[] = $row; }
$out=array('mod'=>'kelulusan','data'=>$data,'fld'=>$col);

foreach ($data as $d) {
  // var_dump($data);
  $id_peserta=$d['id_peserta'];
  $tanggal_pengujian=$d['tanggal_pengujian'];
  $sql2="
  SELECT count(id) as banyakData FROM nilai WHERE id_peserta='$id_peserta'
       AND tanggal_pengujian='$tanggal_pengujian'
   ";
   $result_cek = $conn->query($sql2);
    $row_cek = $result_cek->fetch();
   // var_dump($row_cek['banyakData']);
     if($row_cek['banyakData'] == 4 ){
       $sql_delete = "DELETE FROM tb_pendaftaran_peserta
                      WHERE id_peserta = '$id_peserta'  AND jadwal_ujian = '$tanggal_pengujian'";
                      $conn->query($sql_delete);
     }
     // var_dump(count($banyak));


  //cek jika datanya sudah ada pada tabel kelulusan dengan id_peserta dan jadwal_pengujian, jika ada yang sama
  //maka update, jika tidak ada yang sama maka insert
  $sql_cek="SELECT * FROM tb_kelulusan
                     WHERE id_peserta = '$d[id_peserta]'
                     AND tanggal_pengujian = '$d[tanggal_pengujian]'";
   $result_cek = $conn->query($sql_cek);
   $row_cek = $result_cek->fetch();

   if($row_cek){
      //jika sama
      $sql_update =" UPDATE tb_kelulusan
                            SET  nama_peserta = '$d[nama]', nilai_akumulatif = '$d[nilai_akumulatif]',
                            sabuk_peserta = '$d[sabuk]', keterangan_lulus = '$d[lulus]'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update);
   }else{
     //jika tidak sama
     $sql_insert =" INSERT INTO tb_kelulusan SET id_peserta = '$d[id_peserta]', nama_peserta = '$d[nama]',
                     tanggal_pengujian = '$d[tanggal_pengujian]', nilai_akumulatif = '$d[nilai_akumulatif]',
                     sabuk_peserta = '$d[sabuk]', keterangan_lulus = '$d[lulus]'" ;
     $conn->query($sql_insert);
   }

   // cek ke tabel history ujian
    $sql_cek_history="SELECT * FROM tb_history_peserta_ujian
                      WHERE id_peserta = '$d[id_peserta]'
                      AND tanggal_pengujian = '$d[tanggal_pengujian]'";
    $result_cek_history = $conn->query($sql_cek_history);
    $row_cek_history = $result_cek_history->fetch();
    if($row_cek_history){
         $sekarang=date('Y-m-d');
       //jika sama
       $sql_update_history ="UPDATE tb_history_peserta_ujian
                             SET  nilai_akumulatif = '$d[nilai_akumulatif]',
                             sabuk_akhir = '$d[sabuk]', status_kelulusan = '$d[lulus]', updated_at = '$sekarang'
                             WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
       $conn->query($sql_update_history);
    }
    //update kenaikan sabuk jika LULUS
    $sql_cek_sabuk="SELECT * FROM tb_history_peserta_ujian
                      WHERE id_peserta = '$d[id_peserta]'
                      AND tanggal_pengujian = '$d[tanggal_pengujian]'
                      AND status_kelulusan = 'LULUS'";
    $result_cek_sabuk = $conn->query($sql_cek_sabuk);
    $row_cek_sabuk = $result_cek_sabuk->fetch();

    if($row_cek_sabuk['sabuk_awal'] == 'Putih' || $row_cek_sabuk['sabuk_awal'] == 'putih'){
      //update sabuk di tb_history_peserta_ujian
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Kuning'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Kuning'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Kuning'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);

    }else if($row_cek_sabuk['sabuk_awal'] == 'Kuning' || $row_cek_sabuk['sabuk_awal'] == 'kuning'){
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Jingga'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Jingga'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Jingga'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);

    }else if($row_cek_sabuk['sabuk_awal'] == 'Jingga'|| $row_cek_sabuk['sabuk_awal'] == 'jingga'){
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Hijau'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Hijau'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Hijau'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);

    }else if($row_cek_sabuk['sabuk_awal'] == 'Hijau' || $row_cek_sabuk['sabuk_awal'] == 'hijau'){
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Biru'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Biru'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Biru'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);

    }else if($row_cek_sabuk['sabuk_awal'] == 'Biru' || $row_cek_sabuk['sabuk_awal'] == 'biru'){
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Coklat'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Coklat'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Coklat'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);

    }else if($row_cek_sabuk['sabuk_awal'] == 'Coklat' || $row_cek_sabuk['sabuk_awal'] == 'coklat'){
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Hitam1'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Hitam1'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Hitam1'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);

    }else if($row_cek_sabuk['sabuk_awal'] == 'Hitam1' || $row_cek_sabuk['sabuk_awal'] == 'hitam1'){
      $sql_update_sabuknya ="UPDATE tb_history_peserta_ujian
                            SET sabuk_akhir = 'Hitam2'
                            WHERE id_peserta = '$d[id_peserta]' AND tanggal_pengujian = '$d[tanggal_pengujian]' ";
      $conn->query($sql_update_sabuknya);
      //update sabuk di tabel peserta
      $sql_update_sabuknya2 ="UPDATE peserta
                            SET sabuk = 'Hitam2'
                            WHERE id = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya2);
      //update sabuk di tb_kelulusan
      $sql_update_sabuknya3 ="UPDATE tb_kelulusan
                            SET sabuk_akhir = 'Hitam2'
                            WHERE id_peserta = '$d[id_peserta]'";
      $conn->query($sql_update_sabuknya3);
    }

}

// die;


echo json_encode($out);

}




}

?>
