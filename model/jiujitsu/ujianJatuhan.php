  <?php
  class mod extends ktupad {
  public $conf2= array(
    'tb' => 'tb_pendaftaran_peserta',
    'mn' => 'ujianJatuhan',
    );


  public function kuis() {
  $d=$this->conf;
  $datas=$d['data'];
  // $arr = json_decode($jawab, TRUE);

  $sabuk=$datas['sabuk'];
  $data=array();
  $conn=$this->connect();
  $result = $conn->query("SELECT id, nama, pertanyaan, $sabuk as jawaban, status FROM kuis WHERE $sabuk!=''");
  $result->setFetchMode(PDO::FETCH_ASSOC);
  while($row = $result->fetch()) { $data[] = $row; }
  $out=array('mod'=>'gerakan','data'=>$data);
  echo json_encode($out);
  }

  public function kirimjatuh(){
    $d=$this->conf;
    $datas=$d['data'];
    $riwayats=$d['riwayat'];
    $statusPenguji=$d['statusPenguji'];

    $conn=$this->connect();
    $conn->query("SET SESSION sql_mode = ''");

    $obj=array();
    $wow=array();

    //menyiapkan untuk insert ke tabel nilai
    foreach($datas as $key => $val) {  $obj[]=$key."='".$val."' "; }

    $keterangan=$datas['keterangan'];
    $sabuk=$datas['sabuk'];
  // $jawab=$jawab[1];

    $arr = json_decode($keterangan, TRUE);
    $c=0;


  $result = $conn->query("SELECT sabuk, jatuhan FROM param WHERE sabuk='$sabuk'");
  $result->setFetchMode(PDO::FETCH_ASSOC);
  // while($row = $result->fetch()) { $data[] = $row; }
  $row = $result->fetch() ;

  $perolehan=$keterangan;
  if($sabuk=='putih'){
    $total=$row['jatuhan'];
    $nilai=$keterangan;
    $pembagi=30;
    $perolehan=($nilai/$total)*$pembagi;
   }

   $sekarang=date('Y-m-d');

  // $nilai=$datas['nilai'];
    $obj[]='nilai='.$perolehan;
    $orow = implode(',',$obj);

    $sekarang=date('Y-m-d');
    $users=$datas['users'];
    $peserta=$datas['peserta'];
    $id_peserta=$datas['id_peserta'];
    // $nama_peserta=$datas['nama_peserta'];
    $sabuk=$datas['sabuk'];
    $tanggal_pengujian=$riwayats['tanggal_pengujian'];
    $nilai=$perolehan;
    $keterangan=$datas['keterangan'];
    $status='jatuhan';

    if($statusPenguji == 'penguji1'){
      $sql="
      SELECT id FROM nilai WHERE users = '$users'
           AND status_penguji = '$statusPenguji'
           AND id_peserta='$id_peserta'
           AND sabuk='$sabuk'
           AND status='$status'
           LIMIT 1;
       ";
       $result = $conn->query($sql);
       $row = $result->fetch();
       if($row){
           $sql =" update nilai set status_penguji = 'penguji1', status='jatuhan', updated_at='$sekarang', tanggal_pengujian='$tanggal_pengujian', ".$orow." where id_peserta = '$id_peserta' and status = 'jatuhan' and status_penguji = 'penguji1'" ;
           $conn->query($sql);
         }else{
           $sql =" insert into nilai set status_penguji = 'penguji1', status='jatuhan', tanggal='$sekarang', created_at='$sekarang', tanggal_pengujian='$tanggal_pengujian', ".$orow ;
           $conn->query($sql);
         }
    }else{
      $sql="
      SELECT id FROM nilai WHERE users = '$users'
           AND status_penguji = '$statusPenguji'
           AND id_peserta='$id_peserta'
           AND sabuk='$sabuk'
           AND status='$status'
           LIMIT 1;
       ";
       $result = $conn->query($sql);
       $row = $result->fetch();
       if($row){
           $sql =" update nilai set status_penguji = 'penguji2', status='jatuhan', updated_at='$sekarang', tanggal_pengujian='$tanggal_pengujian', ".$orow." where id_peserta = '$id_peserta' and status = 'jatuhan' and status_penguji = 'penguji2'" ;
           $conn->query($sql);
         }else{
           $sql =" insert into nilai set status_penguji = 'penguji2', status='jatuhan', tanggal='$sekarang', created_at='$sekarang', tanggal_pengujian='$tanggal_pengujian', ".$orow ;
           $conn->query($sql);
    }
  }

    //menyiapkan untuk insert atau update ke tabel history
    $obj=array();
    foreach($riwayats as $key => $val) {
        $obj[]=$key."='".$val."' ";
      }
      // var_dump($obj);
      // die;


    if($statusPenguji == "penguji1"){

    $obj[]='nilai_akhir_jatuhan1='.$perolehan;
    $orow = implode(',',$obj);

    $sql2="
    SELECT id FROM tb_history_peserta_ujian WHERE id_peserta='$id_peserta'
         AND sabuk_awal='$sabuk'
         AND tanggal_pengujian='$tanggal_pengujian'
      LIMIT 1;
     ";
     $result = $conn->query($sql2);
     $row2 = $result->fetch();
     if($row2){
         $sql =" update tb_history_peserta_ujian set status_ujian='', updated_at='$sekarang', ".$orow." where id_peserta = '$id_peserta'" ;
         $conn->query($sql);
       }else{
         $sql =" insert into tb_history_peserta_ujian set status_ujian='', created_at='$sekarang',".$orow;
         $conn->query($sql);
       }
    $out=array('mod'=>'Add data Jatuhan','data'=>$perolehan);
    echo json_encode($out);

  }else{

    $obj[]='nilai_akhir_jatuhan2='.$perolehan;
    $orow = implode(',',$obj);

    $sql2="
    SELECT id FROM tb_history_peserta_ujian WHERE id_peserta='$id_peserta'
         AND sabuk_awal='$sabuk'
         AND tanggal_pengujian='$tanggal_pengujian'
      LIMIT 1;
     ";
     $result = $conn->query($sql2);
     $row2 = $result->fetch();
     if($row2){
         $sql =" update tb_history_peserta_ujian set status_ujian='', updated_at='$sekarang', ".$orow." where id_peserta = '$id_peserta'" ;
         $conn->query($sql);
       }else{
         $sql =" insert into tb_history_peserta_ujian set status_ujian='', created_at='$sekarang',".$orow;
         $conn->query($sql);
       }
    $out=array('mod'=>'Add data Jatuhan','data'=>$perolehan);
    echo json_encode($out);
  }



  }
}
?>
